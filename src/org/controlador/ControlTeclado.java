package org.controlador;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import org.personagem.Posicionamento;

public class ControlTeclado extends ControlePadrao implements ControladorPersonagem, KeyListener {
	
	protected final int TeclaCima = KeyEvent.VK_UP;
	protected final int TeclaBaixo = KeyEvent.VK_DOWN;
	protected final int TeclaEsquerda = KeyEvent.VK_LEFT;
	protected final int TeclaDireita = KeyEvent.VK_RIGHT;
	protected final int TeclaSegurar = KeyEvent.VK_Q;
	protected final int TeclaPoder1 = KeyEvent.VK_S;
	protected final int TeclaPoder2 = KeyEvent.VK_W;
	protected final int TeclaPoder3 = KeyEvent.VK_D;
	protected final int TeclaBomba = KeyEvent.VK_A;
	
	@Override
	public void keyPressed(KeyEvent K) {
		switch(K.getKeyCode()){
			case TeclaCima : PersonagemgoCima(); break;
			case TeclaBaixo : PersonagemgoBaixo(); break;
			case TeclaEsquerda : PersonagemgoEsquerda(); break;
			case TeclaDireita : PersonagemgoDireita(); break;
			case TeclaSegurar : PersonagemSegurar(); break;
			case TeclaPoder1 : PersonagemPoder1(); break;
			case TeclaPoder2 : PersonagemPoder2(); break;
			case TeclaPoder3 : PersonagemPoder3(); break;
			case TeclaBomba : PersonagemSoltarBomba(); break;
		}

	}

	@Override
	public void keyReleased(KeyEvent K) {
		switch(K.getKeyCode()){
			case TeclaSegurar : PersonagemLancar(); break;
			case TeclaCima : PersonagempararCima(); break;
			case TeclaBaixo : PersonagempararBaixo(); break;
			case TeclaEsquerda : PersonagempararEsquerda(); break;
			case TeclaDireita : PersonagempararDireita(); break;
		}
	}

	private void PersonagempararDireita() {
		if(Person.getPosicionamento()==Posicionamento.DIREITA)
			Person.setPosicionamento(Posicionamento.PARADO);
		
	}

	private void PersonagempararEsquerda() {
		if(Person.getPosicionamento()==Posicionamento.ESQUERDA)
			Person.setPosicionamento(Posicionamento.PARADO);
		
	}

	private void PersonagempararBaixo() {
		if(Person.getPosicionamento()==Posicionamento.BAIXO)
			Person.setPosicionamento(Posicionamento.PARADO);
		
	}

	private void PersonagempararCima() {
		if(Person.getPosicionamento()==Posicionamento.CIMA)
			Person.setPosicionamento(Posicionamento.PARADO);
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
	}

}
