package org.elementos;

import org.desenho.ParedeInquebravelRenderer;
import org.terreno.CelulaTerreno;

public class ParedeInquebravel extends ElementoTerrenoPadrao {

	public ParedeInquebravel(CelulaTerreno CT){
		super(CT);
		Renderizador = new ParedeInquebravelRenderer(this);
		tipo = TiposElementos.paredeInquebravel;
	}

	public ParedeInquebravel() {
		// TODO Auto-generated constructor stub
	}

}
