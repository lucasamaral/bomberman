package org.ovos;

import org.personagem.Personagem;
import org.personagem.PersonagemOvoYoshi;
import org.terreno.CelulaTerreno;

public class OvoYoshi extends OvoBasico {

	public OvoYoshi(CelulaTerreno CT) {
		super(CT);
	}
	
	@Override
	public Personagem gerarPersonagem(Personagem p) {
		return new PersonagemOvoYoshi(p);
	}

}
