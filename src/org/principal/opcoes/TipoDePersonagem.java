package org.principal.opcoes;

import org.personagem.IdentificacaoPersonagem;

public class TipoDePersonagem {
	public final static String pateta = "pateta";
	public final static String mario = "mario";
	public final static String whiteBomberman = "bomberman";
	public final static String blackBomberman = "blackBomberman";
	public final static String pikachu = "pikachu";
	public final static String none = "def";

	private IdentificacaoPersonagem selecionado;

	public TipoDePersonagem(IdentificacaoPersonagem selec) {
		selecionado = selec;
	}
	
	public void setSelecionado(IdentificacaoPersonagem selec) throws Exception {
		selecionado = selec;
	}

	public IdentificacaoPersonagem getSelecionado() {
		return selecionado;
	}
}
