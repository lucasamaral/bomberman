package org.principal.opcoes;

import org.fase.IdentificacaoFase;

public class TipoDeFase {
	
	private IdentificacaoFase selecionada = null;
	
	public IdentificacaoFase getSelecionada(){
		return selecionada;
	}
	
	public void setSelecionada(IdentificacaoFase valor) throws Exception{
		selecionada = valor;
	}
}
