package org.principal.opcoes;

public class Situacao {
	public final static String menuPrincipal = "menuPrincipal"; 
	public final static String menuPersonagem = "menuPersonagem";
	public final static String menuFase = "menuFase";
	public final static String jogando = "jogando";
	public final static String creditos = "creditos";
	
	private String atual;
	
	public Situacao(String situacao) {
		atual = situacao;
	}
	
	public void setSituacao(String situacao) throws Exception{
		switch (situacao) {
		case menuPrincipal:
			atual = menuPrincipal;
			break;
		case menuPersonagem:
			atual = menuPersonagem;
			break;
		case jogando:
			atual = jogando;
			break;
		case creditos:
			atual = creditos;
			break;
		case menuFase:
			atual = menuFase;
			break;
		default:
			throw new Exception("Situa��o Inesperada: " + situacao);
		}
	}
	
	public String getSituacao(){
		return atual;
	}
	
}

