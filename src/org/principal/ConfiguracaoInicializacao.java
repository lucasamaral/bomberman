package org.principal;

import java.util.List;

import org.fase.FabricadeFaseAbstrata;
import org.personagem.IdentificacaoPersonagem;

public class ConfiguracaoInicializacao {
	
	private FabricadeFaseAbstrata FF;
	private List<IdentificacaoPersonagem> Personas;
	
	public FabricadeFaseAbstrata getFF() {
		return FF;
	}
	public void setFF(FabricadeFaseAbstrata fF) {
		FF = fF;
	}
	public List<IdentificacaoPersonagem> getPersonas() {
		return Personas;
	}
	public void setPersonas(List<IdentificacaoPersonagem> personas) {
		Personas = personas;
	}
	
}
