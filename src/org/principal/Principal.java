package org.principal;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.controlador.ControlTeclado;
import org.controlador.ControladorArtificial;
import org.controlador.ControlePadrao;
import org.desenho.menu.Creditos;
import org.desenho.menu.MenuFase;
import org.desenho.menu.MenuPersonagem;
import org.desenho.menu.MenuPrincipal;
import org.fase.FabricaDeFabricaDeFase;
import org.fase.FabricadeFaseAbstrata;
import org.fase.IdentificacaoFase;
import org.inteligenciaartificial.BigAIBrain;
import org.mecanica.MecanicaJogo;
import org.personagem.IdentificacaoPersonagem;
import org.personagem.fabricas.FabricaPersonagem;
import org.principal.opcoes.Situacao;

public class Principal {

	/**
	 * @param args
	 * @throws ExecutionException
	 * @throws InterruptedException
	 */

	public static void main(String[] args) throws InterruptedException,
			ExecutionException {
		final ExecutorService ES = Executors.newCachedThreadPool();
		Situacao situacao = new Situacao("menuPrincipal");
		
		ControlTeclado CT = new ControlTeclado();
		FabricadeFaseAbstrata fabrica;
		MecanicaJogo MJ = null;
		BigAIBrain BBB = null;
		

		Jogo game = null;
		
		MenuPrincipal menu = new MenuPrincipal(situacao);
		menu.setVisible(true);
		menu.desativaInicio();
		menu.desativaPersonagem();
		
		Creditos cred = new Creditos();
		cred.setVisible(false);
		
		MenuPersonagem menuPers = new MenuPersonagem(IdentificacaoPersonagem.none);
		menuPers.setVisible(false);
		
		MenuFase menuFase = new MenuFase();
		menuFase.setVisible(false);

		JPanel container = new JPanel();
		container.setLayout(new CardLayout(2, 2));

		container.add(menu);
		container.add(menuPers);
		container.add(cred);
		container.add(menuFase);

		JFrame topFrame = criaFrame(ES);
		topFrame.add(container);
		topFrame.pack();
		topFrame.setSize(new Dimension(500, 500));
		topFrame.addKeyListener(CT);
		topFrame.setVisible(true);

		Situacao antiga = new Situacao(situacao.getSituacao());
		boolean mudou = false;
		boolean escolheuFase = false;
		boolean escolheuPers = false;
		while (true) {
			Thread.sleep(50);
			while (!mudou) {
				if(situacao.getSituacao().equals(Situacao.jogando) && MJ.FimdeJogo()){
					try {
						situacao.setSituacao(Situacao.creditos);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					Thread.sleep(2000);
				}
				if (!antiga.getSituacao().equals(situacao.getSituacao())) {
					mudou = true;
					try {
						antiga.setSituacao(situacao.getSituacao());
					} catch (Exception e) {
						e.printStackTrace();
					}
					switch (situacao.getSituacao()) {
					case Situacao.menuPrincipal:
						menu.setVisible(true);
						menuFase.setVisible(false);
						menuPers.setVisible(false);
						cred.setVisible(false);
						menu.requestFocus();
						break;
					case Situacao.jogando:
						game = new Jogo(MJ);
						game.addKeyListener(CT);
						container.add(game);
						ES.submit(MJ);
						ES.submit(game);
						game.setVisible(true);
						menuFase.setVisible(false);
						menu.setVisible(false);
						menuPers.setVisible(false);
						cred.setVisible(false);
						game.requestFocus();
						break;
					case Situacao.menuPersonagem:
						menuPers.setVisible(true);
						cred.setVisible(false);
						menuFase.setVisible(false);
						menu.setVisible(false);
						menuPers.requestFocus();
						break;
					case Situacao.menuFase:
						menuFase.setVisible(true);
						menuPers.setVisible(false);
						cred.setVisible(false);
						menu.setVisible(false);
						menuFase.requestFocus();
						break;
					case Situacao.creditos:
						cred.setVisible(true);
						menuPers.setVisible(false);
						menu.setVisible(false);
						menuFase.setVisible(false);
						game.setVisible(false);
						ES.shutdownNow();
					default:
						break;
					}
				}
			}
			if (mudou) {
				topFrame.pack();
				topFrame.revalidate();
				topFrame.setSize(new Dimension(500, 500));
				topFrame.setVisible(true);
				mudou = false;
			}
			if (situacao.getSituacao().equals(Situacao.menuPersonagem)) {
				IdentificacaoPersonagem[] Ids = menuPers.getIdPersonagem();
				escolheuPers = true;
				if(escolheuFase && escolheuPers)
					menu.ativaInicio();
				menu.desativaPersonagem();
				ControladorArtificial CA = new ControladorArtificial(BBB);
				ControladorArtificial CB = new ControladorArtificial(BBB);
				ControladorArtificial CC = new ControladorArtificial(BBB);
				definirPersonagem(Ids[0], CT);
				definirPersonagem(Ids[1], CA);
				definirPersonagem(Ids[2], CB);
				definirPersonagem(Ids[3], CC);
				MJ.adicionarControle(CT);
				MJ.adicionarControle(CA);
				MJ.adicionarControle(CB);
				MJ.adicionarControle(CC);
				mudou = true;
				try {
					situacao.setSituacao(Situacao.menuPrincipal);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if(situacao.getSituacao().equals(Situacao.menuFase)){
				escolheuFase = true;
				if(escolheuFase && escolheuPers)
					menu.ativaInicio();
				menu.desativaMenuFase();
				menu.ativaPersonagem();
				fabrica = definirFase(menuFase.getIdFase());
				MJ = new MecanicaJogo(fabrica, ES);
				BBB = new BigAIBrain(MJ);
				mudou = true;
				try {
					situacao.setSituacao(Situacao.menuPrincipal);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
		}

	}

	private static FabricadeFaseAbstrata definirFase(IdentificacaoFase tipoFase) {
		FabricaDeFabricaDeFase fabfab = new FabricaDeFabricaDeFase();
		FabricadeFaseAbstrata fab = fabfab.fazerFase(tipoFase);
		return fab;
		
	}

	private static void definirPersonagem(IdentificacaoPersonagem esc,
			ControlePadrao ctrl) {
		FabricaPersonagem factory = new FabricaPersonagem();
		ctrl.setPersonagem(factory.criaPersonagem(esc,"Player 1"));
	}

	private static JFrame criaFrame(final ExecutorService exec) {
		JFrame f = new JFrame("Bomberman");
		f.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}

			@Override
			public void windowDeiconified(WindowEvent e) {
				exec.shutdownNow();
			}

			@Override
			public void windowIconified(WindowEvent e) {
			}
		});
		return f;
	}

}
