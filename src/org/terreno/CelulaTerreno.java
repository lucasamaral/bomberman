package org.terreno;

import org.desenho.CelulaTerrenoRenderer;
import org.elementos.ElementoTerreno;
import org.elementos.Fogo;
import org.elementos.TiposElementos;

public interface CelulaTerreno{

	public TiposElementos getEstado();
	public void adicionarElemento(ElementoTerreno ET);
	public void removerElemento(ElementoTerreno B);
	public ElementoTerreno getTopo();
	public int getColuna();
	public int getLinha();
	public Fogo atearFogo();
	void apagarFogo(Fogo F);
	public CelulaTerrenoRenderer getRenderer();
	public int getLargura();
	public int getAltura();
	public Coordenada getCoordenadaCorrespondente();
	public ElementoTerreno tirarTopo();
}
