package org.terreno;

import java.util.Stack;

import org.desenho.CelulaTerrenoRenderer;
import org.elementos.ElementoTerreno;
import org.elementos.Fogo;
import org.elementos.TiposElementos;

public class CelulaTerrenoPadrao implements CelulaTerreno{
	private Stack<ElementoTerreno> CoisasEmCima;
	private int linha;
	private int coluna;
	private int largura;
	private int altura;
	private CelulaTerrenoRenderer Renderizador;
	
	public CelulaTerrenoPadrao(){
		CoisasEmCima = new Stack<ElementoTerreno>();
	}
	
	public CelulaTerrenoPadrao(int l,int c){
		this();
		linha = l;
		coluna = c;
	}
	
	public CelulaTerrenoPadrao(int j, int i, int largura2, int altura2) {
		this(j,i);
		largura = largura2;
		altura = altura2;
		Renderizador = new CelulaTerrenoRenderer(this);
	}

	@Override
	public ElementoTerreno getTopo(){
		synchronized (CoisasEmCima) {
			if(!CoisasEmCima.isEmpty())
				return CoisasEmCima.peek();
			return null;
		}
	}

	@Override
	public TiposElementos getEstado() {
		synchronized (CoisasEmCima) {
			if(CoisasEmCima.isEmpty())
				return TiposElementos.chao;	
			return CoisasEmCima.peek().getTipo();
			}
		}
	
	public void adicionarElemento(ElementoTerreno ET) {
		synchronized (CoisasEmCima) {
			CoisasEmCima.push(ET);
		}
		ET.getRenderer().setAltura(altura);
		ET.getRenderer().setLargura(largura);
		ET.setCelulaAtual(this);
	}

	@Override
	public void removerElemento(ElementoTerreno B) {
		if (B!=null) {
			synchronized (CoisasEmCima) {
				CoisasEmCima.remove(B);
			}
			if(B.getCelulaAtual()==this)
				B.setCelulaAtual(null);
		}
		
	}

	@Override
	public int getColuna() {
		return coluna;
	}

	@Override
	public int getLinha() {
		return linha;
	}

	@Override
	public Fogo atearFogo() {
		Fogo F = new Fogo(this);
		if(!CoisasEmCima.isEmpty())
			CoisasEmCima.pop();
		adicionarElemento(F);
		return F;
		
	}

	@Override
	public void apagarFogo(Fogo F) {
		CoisasEmCima.remove(F);
	}
	
	@Override
	public String toString(){
		return coluna+" - "+linha;
	}

	@Override
	public CelulaTerrenoRenderer getRenderer() {
		return Renderizador;
	}

	@Override
	public int getLargura() {
		return largura;
	}

	@Override
	public int getAltura() {
		return altura;
	}

	@Override
	public Coordenada getCoordenadaCorrespondente() {
		return new Coordenada(coluna*largura,linha*altura);
	}

	@Override
	public ElementoTerreno tirarTopo() {
		if(!CoisasEmCima.isEmpty())
			synchronized (CoisasEmCima) {
				return CoisasEmCima.pop();
			}
		return null;
	}
}
