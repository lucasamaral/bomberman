package org.bomba;

import org.personagem.Personagem;
import org.terreno.Coordenada;

public abstract class FabricadeBombas {

	public abstract Bomba getBomba(int poder,Coordenada C);

	public abstract Bomba getBomba(int poderdeFogo, Coordenada clone,
			Personagem personagemPadrao);
}
