package org.bomba;

import org.personagem.Personagem;
import org.terreno.Coordenada;

public class FabricaBombaControleRemoto extends FabricadeBombas{

	@Override
	public Bomba getBomba(int poder, Coordenada C) {
		return new BombaControleRemoto(poder,C);
	}

	@Override
	public Bomba getBomba(int poderdeFogo, Coordenada clone,
			Personagem personagemPadrao) {
		return new BombaControleRemoto(poderdeFogo,clone,personagemPadrao);
	}

}
