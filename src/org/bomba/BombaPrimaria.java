package org.bomba;

import java.util.LinkedHashSet;
import java.util.Set;

import org.cenario.Cenario;
import org.desenho.BombaRenderer;
import org.desenho.ElementoTerrenoRenderer;
import org.elementos.TiposElementos;
import org.personagem.Personagem;
import org.terreno.CelulaTerreno;
import org.terreno.Coordenada;

public class BombaPrimaria implements Bomba {
	protected int Poder;
	protected Coordenada Localizacao;
	protected Personagem lancador;
	protected BombaRenderer Renderizador = new BombaRenderer(this);
	protected CelulaTerreno CT;
	protected boolean explodida=false;
	
	@Override
	public ElementoTerrenoRenderer getRenderer(){
		return Renderizador;
	}
	
	public BombaPrimaria(){
	}
	
	public BombaPrimaria(int poder,Coordenada C){
		Localizacao = C;
		Poder=poder;
	}
	
	public BombaPrimaria(int poder, Coordenada C,Personagem P) {
		Localizacao = C;
		Poder=poder;
		lancador = P;
	}

	@Override
	public TiposElementos getTipo() {
		return TiposElementos.bomba;
	}

	@Override
	public Set<CelulaTerreno> CelulasExplodidas(Cenario C) {
		Set<CelulaTerreno> Lista = new LinkedHashSet<CelulaTerreno>();
		CelulaTerreno Aqui = C.CasaCorrespondente(Localizacao);
		Lista.add(Aqui);
		for(CelulaTerreno CT=Aqui;C.distancia(CT, Aqui)<=Poder;CT=C.CelulaAcima(CT)){
			if(queima(CT.getEstado()) && CT!=Aqui){
				Lista.add(CT);
			}
			if(!atravessaEstado(CT.getEstado())){
				break;
			}
		}
		for(CelulaTerreno CT=Aqui;C.distancia(CT, Aqui)<=Poder;CT=C.CelulaEsquerda(CT)){
			if(queima(CT.getEstado()) && CT!=Aqui){
				Lista.add(CT);
			}
			if(!atravessaEstado(CT.getEstado()) && CT!=Aqui){
				break;
			}
		}
		for(CelulaTerreno CT=Aqui;C.distancia(CT, Aqui)<=Poder;CT=C.CelulaDireita(CT)){
			if(queima(CT.getEstado())){
				Lista.add(CT);
			}
			if(!atravessaEstado(CT.getEstado()) && CT!=Aqui){
				break;
			}
		}
		for(CelulaTerreno CT=Aqui;C.distancia(CT, Aqui)<=Poder;CT=C.CelulaAbaixo(CT)){
			if(queima(CT.getEstado()) && CT!=Aqui){
				Lista.add(CT);
			}
			if(!atravessaEstado(CT.getEstado())){
				break;
			}
		}
		return Lista;
	}

	protected boolean atravessaEstado(TiposElementos estado) {
		return estado==TiposElementos.chao || estado==TiposElementos.fogo;
	}

	protected boolean queima(TiposElementos estado) {
		return estado!=TiposElementos.paredeInquebravel;
	}

	@Override
	public boolean explodeporTempo() {
		return true;
	}

	@Override
	public int TempoExplosao() {
		return 3000;
	}

	@Override
	public Coordenada localizacao() {
		return Localizacao;
	}

	@Override
	public Personagem lancador() {
		return lancador;
	}

	@Override
	public void setCoordenadas(Coordenada nova) {
		Localizacao = nova;
		
	}

	@Override
	public void setCelulaAtual(CelulaTerreno CT) {
		this.CT = CT;
		
	}

	@Override
	public CelulaTerreno getCelulaAtual() {
		return CT;
	}

	@Override
	public int getPoder() {
		return Poder;
	}

	@Override
	public void onSegurado() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void aoSerSolto(CelulaTerreno cT) {
		cT.adicionarElemento(this);
		this.setCelulaAtual(cT);
		this.setCoordenadas(cT.getCoordenadaCorrespondente());
	}
	
	@Override
	public void inicializarLocalizacao(CelulaTerreno CT) {
		setCelulaAtual(CT);
		setCoordenadas(CT.getCoordenadaCorrespondente());
	}

	@Override
	public void setExplodida(boolean b) {
		explodida = b;
		
	}

	@Override
	public boolean foiExplodida() {
		return explodida;
	}

}
