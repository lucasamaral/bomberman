package org.bomba;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.cenario.Cenario;
import org.elementos.TiposElementos;
import org.personagem.Personagem;
import org.terreno.CelulaTerreno;
import org.terreno.Coordenada;

public class BombaMothaF extends BombaPrimaria{

	public BombaMothaF(int poderdeFogo, Coordenada clone,
			Personagem personagemPadrao) {
		super(poderdeFogo,clone,personagemPadrao);
	}

	public BombaMothaF(int poder, Coordenada c) {
		super(poder,c);
	}

	@Override
	public Set<CelulaTerreno> CelulasExplodidas(Cenario C) {
		Set<CelulaTerreno> Lista = new LinkedHashSet<CelulaTerreno>();
		CelulaTerreno Aqui = C.CasaCorrespondente(Localizacao);
		for(List<CelulaTerreno> L : C.getCasas()){
			for(CelulaTerreno CT:L){
				if(C.distancia(Aqui,CT)<=Poder&&CT.getEstado()!=TiposElementos.paredeInquebravel){
					Lista.add(CT);
				}
			}
		}
		return Lista;
	}
	
}
