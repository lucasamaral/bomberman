package org.poder;

import org.bomba.Bomba;
import org.cenario.Cenario;
import org.elementos.TiposElementos;
import org.mecanica.MecanicaJogo;
import org.personagem.Personagem;
import org.terreno.CelulaTerreno;

public class ChutarBomba implements Poder {

	private Personagem Dono;
	
	public ChutarBomba(Personagem p) {
		Dono = p;
	}

	@Override
	public void executar(MecanicaJogo M) {
		Cenario C = M.getCenario();
		CelulaTerreno La = null;
		CelulaTerreno Depois = null;
		CelulaTerreno Aqui = C.CasaCorrespondente(Dono.getCoordenada());
		switch(Dono.getUltimoPosicionamento()){
			case CIMA: 
				La = C.CelulaAcima(Aqui);
				if(La.getEstado()==TiposElementos.bomba){
					Depois = C.CelulaAcima(La);
					while(Depois.getEstado()!=TiposElementos.chao)
						Depois=C.CelulaAcima(Depois);
					C.moverBomba((Bomba) La.getTopo(),La,Depois);
				}
				break;
			case ESQUERDA:
				La = C.CelulaEsquerda(Aqui);
				if(La.getEstado()==TiposElementos.bomba){
					Depois = C.CelulaEsquerda(La);
					while(Depois.getEstado()!=TiposElementos.chao)
						Depois=C.CelulaEsquerda(Depois);
					C.moverBomba((Bomba) La.getTopo(),La,Depois);
				}
				break;
			case DIREITA:
				La = C.CelulaDireita(Aqui);
				if(La.getEstado()==TiposElementos.bomba){
					Depois = C.CelulaDireita(La);
					while(Depois.getEstado()!=TiposElementos.chao)
						Depois=C.CelulaDireita(Depois);
					C.moverBomba((Bomba) La.getTopo(),La,Depois);
				}
				break;
			case BAIXO:
				La = C.CelulaAbaixo(Aqui);
				if(La.getEstado()==TiposElementos.bomba){
					Depois = C.CelulaAbaixo(La);
					while(Depois.getEstado()!=TiposElementos.chao)
						Depois=C.CelulaAbaixo(Depois);
					C.moverBomba((Bomba) La.getTopo(),La,Depois);
				}
				break;
		}
		
	}

}
