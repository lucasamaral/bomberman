package org.poder;

import org.mecanica.MecanicaJogo;

public interface Poder {
	
	public void executar(MecanicaJogo M);
}
