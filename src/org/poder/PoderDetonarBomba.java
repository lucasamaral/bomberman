package org.poder;

import org.bomba.Bomba;
import org.mecanica.MecanicaJogo;
import org.personagem.Personagem;

public class PoderDetonarBomba implements Poder {
	
	private Personagem dono;
	
	public PoderDetonarBomba(Personagem p) {
		dono = p;
	}

	@Override
	public void executar(MecanicaJogo M) {
		Bomba Explodida = null;
		for(Bomba B : dono.getBombasColocadas()){
			if(!B.explodeporTempo()){
				M.getCenario().explodirBomba(B, 1500);
				Explodida = B;
				break;
			}
		}
		dono.getBombasColocadas().remove(Explodida);
	}

}
