package org.poder;

import org.caracteristicas.Seguravel;
import org.mecanica.MecanicaJogo;

public interface PoderSegurar{
	
	public Seguravel iniciar(MecanicaJogo M);
	public void finalizar(MecanicaJogo M);
}
