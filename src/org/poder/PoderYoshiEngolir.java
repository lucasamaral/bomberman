package org.poder;

import org.bomba.Bomba;
import org.cenario.Cenario;
import org.elementos.TiposElementos;
import org.mecanica.MecanicaJogo;
import org.personagem.Personagem;
import org.terreno.CelulaTerreno;

public class PoderYoshiEngolir implements Poder {

	Personagem dono;
	
	public PoderYoshiEngolir(Personagem P){
		dono = P;
	}
	
	@Override
	public void executar(MecanicaJogo M) {
		Cenario C = M.getCenario();
		CelulaTerreno La = null;
		CelulaTerreno Aqui = C.CasaCorrespondente(dono.getCoordenada());
		switch(dono.getUltimoPosicionamento()){
			case CIMA: 
				La = C.CelulaAcima(Aqui);
				break;
			case ESQUERDA:
				La = C.CelulaEsquerda(Aqui);
				break;
			case DIREITA:
				La = C.CelulaDireita(Aqui);
				break;
			case BAIXO:
				La = C.CelulaAbaixo(Aqui);
				break;
		}
		if(La.getEstado()==TiposElementos.bomba){
			M.pararBomba((Bomba) La.getTopo());
			La.removerElemento(La.getTopo());
		}
	}

}
