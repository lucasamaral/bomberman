package org.fase;

import java.util.HashMap;
import java.util.Map;

public class FabricaDeFabricaDeFase {
	
	private Map<IdentificacaoFase,FabricadeFaseAbstrata> Fases = new HashMap<>();
	
	{
		Fases.put(IdentificacaoFase.primeiraFase, new FabricaPrimeiraFase());
		Fases.put(IdentificacaoFase.segundaFase, new FabricaSegundaFase());
		Fases.put(IdentificacaoFase.faseLimpa, new FabricaFaseLimpa());
	}
	
	public FabricadeFaseAbstrata fazerFase(IdentificacaoFase escol){
		if(!Fases.containsKey(escol)){
			return null;
		}
		return Fases.get(escol);
	}

}
