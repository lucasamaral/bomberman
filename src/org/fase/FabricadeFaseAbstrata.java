package org.fase;

import java.util.Collection;
import java.util.List;

import org.cenario.Cenario;
import org.personagem.Personagem;
import org.terreno.CelulaTerreno;
import org.terreno.Coordenada;

public abstract class FabricadeFaseAbstrata {

	public abstract Cenario getCenario();
	public abstract List<Coordenada> getPosicoesInicio();
	public abstract Collection<CelulaTerreno> getPosicoesProibidas(Cenario C);
	public abstract String getNome();
	public abstract void inicializarPersonagem(Personagem P);
}
