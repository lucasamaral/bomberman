package org.fase;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.cenario.Cenario;
import org.cenario.CenarioPadrao;
import org.cenario.MontadorCenario;
import org.elementos.ParedeQuebravel;
import org.ovos.OvoBasico;
import org.ovos.OvoYoshi;
import org.personagem.Personagem;
import org.quadradopoder.QAtravessarParede;
import org.quadradopoder.QBombaControleRemoto;
import org.quadradopoder.QBombaEspinho;
import org.quadradopoder.QBombaMothaF;
import org.quadradopoder.QChutarBomba;
import org.quadradopoder.QEmpurrarBomba;
import org.quadradopoder.QPoderBomba;
import org.quadradopoder.QPoderFogo;
import org.quadradopoder.QPoderVelocidade;
import org.quadradopoder.QSegurarBomba;
import org.terreno.CelulaTerreno;
import org.terreno.Coordenada;

public class FabricaSegundaFase extends FabricadeFaseAbstrata {

	@Override
	public Cenario getCenario() {
		Cenario novo = new CenarioPadrao(21,21,20,20);
		MontadorCenario.adicionarBordas(novo);
		MontadorCenario.adicionarMeios(novo);
		adicionarExtras(novo);
		adicionarParedesQuebraveis(novo);
		return novo;
	}

	private void adicionarParedesQuebraveis(Cenario novo) {
		for(CelulaTerreno C : novo.getEspeciaisDescobertos()){
			C.adicionarElemento(new ParedeQuebravel(C));
		}
		List<CelulaTerreno> CTs = novo.getChaos();
		java.util.Collections.shuffle(CTs);
		Collection<CelulaTerreno> Proibidas = getPosicoesProibidas(novo);
		for(int i=0;i<3*CTs.size()/4;i++){
			if(!novo.celulaPertencea(CTs.get(i),getPosicoesInicio())&&!Proibidas.contains(CTs.get(i)))
				CTs.get(i).adicionarElemento(new ParedeQuebravel(CTs.get(i)));
		}
	}

	private void adicionarExtras(Cenario novo) {
		MontadorCenario.adicionarElemento(novo, new QPoderFogo(), 3, 7);
		MontadorCenario.adicionarElemento(novo, new QPoderFogo(), 17, 1);
		MontadorCenario.adicionarElemento(novo, new QPoderFogo(), 11, 2);
		MontadorCenario.adicionarElemento(novo, new QPoderFogo(), 9, 15);
		MontadorCenario.adicionarElemento(novo, new QPoderFogo(), 19, 4);
		
		novo.CasanoQuadrante(2, 13).adicionarElemento(new QPoderVelocidade(novo.CasanoQuadrante(2, 13)));
		novo.CasanoQuadrante(16, 17).adicionarElemento(new QPoderVelocidade(novo.CasanoQuadrante(16, 17)));
		novo.CasanoQuadrante(8, 3).adicionarElemento(new QPoderVelocidade(novo.CasanoQuadrante(8, 3)));
		novo.CasanoQuadrante(12, 7).adicionarElemento(new QPoderVelocidade(novo.CasanoQuadrante(12, 7)));
		
		novo.CasanoQuadrante(11, 5).adicionarElemento(new QPoderBomba(novo.CasanoQuadrante(11, 5)));
		novo.CasanoQuadrante(2, 13).adicionarElemento(new QPoderBomba(novo.CasanoQuadrante(2, 13)));
		novo.CasanoQuadrante(18, 3).adicionarElemento(new QPoderBomba(novo.CasanoQuadrante(18, 3)));
		novo.CasanoQuadrante(6, 17).adicionarElemento(new QPoderBomba(novo.CasanoQuadrante(6, 17)));
		
		novo.CasanoQuadrante(11, 11).adicionarElemento(new OvoYoshi(novo.CasanoQuadrante(11, 11)));
		novo.CasanoQuadrante(1, 9).adicionarElemento(new OvoBasico(novo.CasanoQuadrante(1, 9)));
		
		novo.CasanoQuadrante(4, 1).adicionarElemento(new QBombaMothaF(novo.CasanoQuadrante(4, 1)));
		novo.CasanoQuadrante(12, 13).adicionarElemento(new QBombaEspinho(novo.CasanoQuadrante(12, 13)));
		novo.CasanoQuadrante(16, 19).adicionarElemento(new QBombaControleRemoto(novo.CasanoQuadrante(16, 19)));
		
		novo.CasanoQuadrante(8, 5).adicionarElemento(new QChutarBomba(novo.CasanoQuadrante(8, 5)));
		novo.CasanoQuadrante(5, 16).adicionarElemento(new QChutarBomba(novo.CasanoQuadrante(5, 16)));
		novo.CasanoQuadrante(10, 15).adicionarElemento(new QSegurarBomba(novo.CasanoQuadrante(10, 15)));
		MontadorCenario.adicionarElemento(novo, new QEmpurrarBomba(), 14, 9);
		MontadorCenario.adicionarElemento(novo, new QAtravessarParede(), 1, 3);
		
	}

	@Override
	public Collection<CelulaTerreno> getPosicoesProibidas(Cenario C){
		List<CelulaTerreno> Lista = new ArrayList<>();
		Lista.add(C.CasanoQuadrante(1, 1));
		Lista.add(C.CasanoQuadrante(2, 1));
		Lista.add(C.CasanoQuadrante(1, 2));
		Lista.add(C.CasanoQuadrante(18, 1));
		Lista.add(C.CasanoQuadrante(19, 2));
		Lista.add(C.CasanoQuadrante(19, 1));
		Lista.add(C.CasanoQuadrante(1, 19));
		Lista.add(C.CasanoQuadrante(2, 19));
		Lista.add(C.CasanoQuadrante(1, 18));
		Lista.add(C.CasanoQuadrante(18, 19));
		Lista.add(C.CasanoQuadrante(19, 19));
		Lista.add(C.CasanoQuadrante(19, 18));
		return Lista;
	}
	
	@Override
	public List<Coordenada> getPosicoesInicio() {
		List<Coordenada> Lista = new ArrayList<Coordenada>();
		Lista.add(new Coordenada(25,25));
		Lista.add(new Coordenada(390,25));
		Lista.add(new Coordenada(25,390));
		Lista.add(new Coordenada(390,390));
		return Lista;
	}
	
	@Override
	public String getNome() {
		return "Segunda Fase";
	}

	@Override
	public void inicializarPersonagem(Personagem P) {
	}

}
