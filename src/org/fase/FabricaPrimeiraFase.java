package org.fase;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.cenario.Cenario;
import org.cenario.CenarioPadrao;
import org.cenario.MontadorCenario;
import org.elementos.ParedeQuebravel;
import org.ovos.OvoYoshi;
import org.personagem.Personagem;
import org.quadradopoder.QAtravessarParede;
import org.quadradopoder.QBombaControleRemoto;
import org.quadradopoder.QBombaEspinho;
import org.quadradopoder.QBombaMothaF;
import org.quadradopoder.QChutarBomba;
import org.quadradopoder.QPoderBomba;
import org.quadradopoder.QPoderFogo;
import org.quadradopoder.QPoderVelocidade;
import org.quadradopoder.QSegurarBomba;
import org.terreno.CelulaTerreno;
import org.terreno.Coordenada;

public class FabricaPrimeiraFase extends FabricadeFaseAbstrata {

	@Override
	public Cenario getCenario() {
		Cenario novo = new CenarioPadrao(21,21,20,20);
		MontadorCenario.adicionarBordas(novo);
		MontadorCenario.adicionarMeios(novo);
		adicionarExtras(novo);
		adicionarParedesQuebraveis(novo);
		return novo;
	}
	
	private void adicionarExtras(Cenario novo) {
		novo.CasanoQuadrante(1, 3).adicionarElemento(new OvoYoshi(novo.CasanoQuadrante(1, 3)));
		novo.CasanoQuadrante(4, 1).adicionarElemento(new QBombaMothaF(novo.CasanoQuadrante(4, 1)));
		novo.CasanoQuadrante(3, 1).adicionarElemento(new QPoderFogo(novo.CasanoQuadrante(3, 1)));
		novo.CasanoQuadrante(11, 1).adicionarElemento(new QPoderVelocidade(novo.CasanoQuadrante(11, 1)));
		novo.CasanoQuadrante(15, 5).adicionarElemento(new QPoderBomba(novo.CasanoQuadrante(15, 5)));
		novo.CasanoQuadrante(1, 5).adicionarElemento(new QChutarBomba(novo.CasanoQuadrante(1, 5)));
		novo.CasanoQuadrante(2, 3).adicionarElemento(new QBombaEspinho(novo.CasanoQuadrante(2, 3)));
		novo.CasanoQuadrante(1, 4).adicionarElemento(new QSegurarBomba(novo.CasanoQuadrante(1, 4)));
		novo.CasanoQuadrante(3, 4).adicionarElemento(new QBombaControleRemoto(novo.CasanoQuadrante(3, 4)));
		novo.CasanoQuadrante(5,2).adicionarElemento(new QAtravessarParede(novo.CasanoQuadrante(5, 2)));
		
	}

	private void adicionarParedesQuebraveis(Cenario novo) {
		for(CelulaTerreno C : novo.getEspeciaisDescobertos()){
			C.adicionarElemento(new ParedeQuebravel(C));
		}
		List<CelulaTerreno> CTs = novo.getChaos();
		java.util.Collections.shuffle(CTs);
		Collection<CelulaTerreno> Proibidas = getPosicoesProibidas(novo);
		for(int i=0;i<3*CTs.size()/4;i++){
			if(!novo.celulaPertencea(CTs.get(i),getPosicoesInicio())&&!Proibidas.contains(CTs.get(i)))
				CTs.get(i).adicionarElemento(new ParedeQuebravel(CTs.get(i)));
		}
	}

	@Override
	public List<Coordenada> getPosicoesInicio() {
		List<Coordenada> Lista = new ArrayList<Coordenada>();
		Lista.add(new Coordenada(22,22));
		Lista.add(new Coordenada(375,23));
		Lista.add(new Coordenada(22,375));
		Lista.add(new Coordenada(382,377));
		return Lista;
	}
	
	@Override
	public Collection<CelulaTerreno> getPosicoesProibidas(Cenario C){
		List<CelulaTerreno> Lista = new ArrayList<>();
		Lista.add(C.CasanoQuadrante(1, 1));
		Lista.add(C.CasanoQuadrante(2, 1));
		Lista.add(C.CasanoQuadrante(1, 2));
		Lista.add(C.CasanoQuadrante(18, 1));
		Lista.add(C.CasanoQuadrante(19, 2));
		Lista.add(C.CasanoQuadrante(19, 1));
		Lista.add(C.CasanoQuadrante(1, 19));
		Lista.add(C.CasanoQuadrante(2, 19));
		Lista.add(C.CasanoQuadrante(1, 18));
		Lista.add(C.CasanoQuadrante(18, 19));
		Lista.add(C.CasanoQuadrante(19, 19));
		Lista.add(C.CasanoQuadrante(19, 18));
		return Lista;
	}
	
	@Override
	public String getNome() {
		return "Primeira Fase";
	}

	@Override
	public void inicializarPersonagem(Personagem P) {
		
	}

}
