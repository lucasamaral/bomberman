package org.fase;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.cenario.Cenario;
import org.cenario.CenarioPadrao;
import org.cenario.MontadorCenario;
import org.ovos.OvoBasico;
import org.ovos.OvoYoshi;
import org.personagem.Personagem;
import org.quadradopoder.QBombaControleRemoto;
import org.quadradopoder.QBombaEspinho;
import org.quadradopoder.QBombaMothaF;
import org.quadradopoder.QChutarBomba;
import org.quadradopoder.QEmpurrarBomba;
import org.quadradopoder.QPoderBomba;
import org.quadradopoder.QPoderFogo;
import org.quadradopoder.QPoderVelocidade;
import org.quadradopoder.QSegurarBomba;
import org.terreno.CelulaTerreno;
import org.terreno.Coordenada;

public class FabricaFaseLimpa extends FabricadeFaseAbstrata {

	@Override
	public Cenario getCenario() {
		Cenario novo = new CenarioPadrao(21,21,20,20);
		MontadorCenario.adicionarBordas(novo);
		MontadorCenario.adicionarMeios(novo);
		adicionarExtras(novo);
		return novo;
	}

	private void adicionarExtras(Cenario novo) {
		List<CelulaTerreno> CTschao = novo.getChaos();
		Collections.shuffle(CTschao);
		
		novo.CasanoQuadrante(2, 13).adicionarElemento(new QPoderVelocidade(novo.CasanoQuadrante(2, 12)));
		novo.CasanoQuadrante(16, 17).adicionarElemento(new QPoderVelocidade(novo.CasanoQuadrante(16, 17)));
		novo.CasanoQuadrante(8, 3).adicionarElemento(new QPoderVelocidade(novo.CasanoQuadrante(8, 3)));
		novo.CasanoQuadrante(12, 7).adicionarElemento(new QPoderVelocidade(novo.CasanoQuadrante(12, 7)));
		
		novo.CasanoQuadrante(11, 11).adicionarElemento(new OvoYoshi(novo.CasanoQuadrante(11, 11)));
		novo.CasanoQuadrante(1, 9).adicionarElemento(new OvoBasico(novo.CasanoQuadrante(1, 9)));
		
		novo.CasanoQuadrante(4, 1).adicionarElemento(new QBombaMothaF(novo.CasanoQuadrante(4, 1)));
		novo.CasanoQuadrante(12, 13).adicionarElemento(new QBombaEspinho(novo.CasanoQuadrante(12, 13)));
		novo.CasanoQuadrante(16, 19).adicionarElemento(new QBombaControleRemoto(novo.CasanoQuadrante(16, 19)));
		
		novo.CasanoQuadrante(8, 5).adicionarElemento(new QChutarBomba(novo.CasanoQuadrante(8, 5)));
		novo.CasanoQuadrante(5, 16).adicionarElemento(new QChutarBomba(novo.CasanoQuadrante(5, 16)));
		novo.CasanoQuadrante(14, 9).adicionarElemento(new QEmpurrarBomba(novo.CasanoQuadrante(14, 9)));
		novo.CasanoQuadrante(10, 15).adicionarElemento(new QSegurarBomba(novo.CasanoQuadrante(10, 15)));
	}

	@Override
	public Collection<CelulaTerreno> getPosicoesProibidas(Cenario C){
		List<CelulaTerreno> Lista = new ArrayList<>();
		Lista.add(C.CasanoQuadrante(1, 1));
		Lista.add(C.CasanoQuadrante(2, 1));
		Lista.add(C.CasanoQuadrante(1, 2));
		Lista.add(C.CasanoQuadrante(18, 1));
		Lista.add(C.CasanoQuadrante(19, 2));
		Lista.add(C.CasanoQuadrante(19, 1));
		Lista.add(C.CasanoQuadrante(1, 19));
		Lista.add(C.CasanoQuadrante(2, 19));
		Lista.add(C.CasanoQuadrante(1, 18));
		Lista.add(C.CasanoQuadrante(18, 19));
		Lista.add(C.CasanoQuadrante(19, 19));
		Lista.add(C.CasanoQuadrante(19, 18));
		return Lista;
	}
	
	@Override
	public List<Coordenada> getPosicoesInicio() {
		List<Coordenada> Lista = new ArrayList<Coordenada>();
		Lista.add(new Coordenada(22,22));
		Lista.add(new Coordenada(375,23));
		Lista.add(new Coordenada(22,375));
		Lista.add(new Coordenada(382,377));
		return Lista;
	}

	@Override
	public String getNome() {
		return "Fase Limpa";
	}
	
	public void inicializarPersonagem(Personagem P){
		for(int i=0;i<10;i++){
			QPoderFogo Q = new QPoderFogo();
			P.adicionarPoder(Q);
			Q.aoAdicionar(P);
		}
		for(int i=0;i<6;i++){
			QPoderBomba B = new QPoderBomba();
			P.adicionarPoder(B);
			B.aoAdicionar(P);
		}
	}

}
