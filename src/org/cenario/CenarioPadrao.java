package org.cenario;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.bomba.Bomba;
import org.desenho.CenarioRenderer;
import org.elementos.Fogo;
import org.elementos.TiposElementos;
import org.mecanica.MecanicaJogo;
import org.personagem.Personagem;
import org.terreno.CelulaTerreno;
import org.terreno.CelulaTerrenoPadrao;
import org.terreno.Coordenada;

public class CenarioPadrao implements Cenario {
	private int AlturadeUmaCasa;
	private int LarguradeUmaCasa;
	private int AlturaemCasas;
	private int LarguraemCasas;
	private List<List<CelulaTerreno>> Casas;
	private List<Personagem> Personagens;
	private MecanicaJogo MecJ;
	private CenarioRenderer desenhoFase= new CenarioRenderer(this);
	
	
	@Override
	public CenarioRenderer getRenderer(){
		return desenhoFase;
	}
	
	public CenarioPadrao(){
		Personagens = new LinkedList<Personagem>();
	}
	
	public CenarioPadrao(int larguraQuadrados,int alturaQuadrados){
		AlturaemCasas = alturaQuadrados;
		LarguraemCasas = larguraQuadrados;
		Personagens = new LinkedList<Personagem>();
		Casas = new ArrayList<List<CelulaTerreno>>();
	}

	public CenarioPadrao(int larguraQuadrados, int alturaQuadrados, int largura, int altura) {
		this(larguraQuadrados,alturaQuadrados);
		LarguradeUmaCasa = largura;
		AlturadeUmaCasa = altura;
		for(int i=0;i<LarguraemCasas;i++){
			List<CelulaTerreno> Coluna = new ArrayList<CelulaTerreno>();
			Casas.add(Coluna);
			for(int j=0;j<AlturaemCasas;j++){
				Coluna.add(new CelulaTerrenoPadrao(j,i,largura,altura));
			}
		}
	}
	
	public void adicionarPersonagem(Personagem novo) {
		Personagens.add(novo);
	}
	
	public CelulaTerreno CasaCorrespondente(Coordenada C){
		int resultx = C.getX()/LarguradeUmaCasa;
		int resulty = C.getY()/AlturadeUmaCasa;
		return CasanoQuadrante(resultx, resulty);
	}
	
	public CelulaTerreno CasanoQuadrante(int x, int y){
		if(x>=LarguraemCasas||y>=AlturaemCasas){
			return null;
		}
		return Casas.get(x).get(y);
	}

	@Override
	public CelulaTerreno CelulaAcima(CelulaTerreno CT) {
		if(CT.getLinha()>0)
			return CasanoQuadrante(CT.getColuna(), CT.getLinha()-1);
		else
			return CasanoQuadrante(CT.getColuna(), LarguraemCasas-1);
	}
	
	@Override
	public CelulaTerreno CelulaAbaixo(CelulaTerreno CT) {
		if(CT.getLinha()<AlturaemCasas-1)
			return CasanoQuadrante(CT.getColuna(), CT.getLinha()+1);
		else
			return CasanoQuadrante(CT.getColuna(), 0);
	}

	public CelulaTerreno CelulaDireita(CelulaTerreno CT) {
		if(CT.getColuna()<LarguraemCasas-1)
			return CasanoQuadrante(CT.getColuna()+1, CT.getLinha());
		else
			return CasanoQuadrante(0, CT.getLinha());
	}

	@Override
	public CelulaTerreno CelulaEsquerda(CelulaTerreno CT) {
		if(CT.getColuna()>0)
			return CasanoQuadrante(CT.getColuna()-1, CT.getLinha());
		else
			return CasanoQuadrante(LarguraemCasas-1, CT.getLinha());
	}

	@Override
	public boolean celulaPertencea(CelulaTerreno celulaTerreno,Collection<Coordenada> L) {
		for(Coordenada C : L){
			int x = C.getX();
			int y = C.getY();
			if(x>=celulaTerreno.getColuna()*LarguradeUmaCasa && x<=(celulaTerreno.getColuna()+1)*LarguradeUmaCasa ){
				if(y>=celulaTerreno.getLinha()*AlturadeUmaCasa && y<=(celulaTerreno.getLinha()+1)*AlturadeUmaCasa){
					return true;
				}
			}
		}
		return false;
	}

	public List<CelulaTerreno> CelulasMesmaColuna(CelulaTerreno CT){
		return Casas.get(CT.getColuna());
	}
	
	public List<CelulaTerreno> CelulasMesmaLinha(CelulaTerreno CT){
		List<CelulaTerreno> CTs = new ArrayList<CelulaTerreno>();
		int linha = CT.getLinha();
		for(int i=0;i<LarguraemCasas;i++){
			CTs.add(CasanoQuadrante(i, linha));
		}
		return CTs;
	}
	
	public List<CelulaTerreno> CelulasVizinhas(CelulaTerreno CT){
		List<CelulaTerreno> CTs = new ArrayList<CelulaTerreno>();
		int linha = CT.getLinha();
		int coluna = CT.getColuna();
		if(linha!=0){
			CTs.add(CasanoQuadrante(coluna,linha-1));
		}
		if(linha!=AlturaemCasas-1){
			CTs.add(CasanoQuadrante(coluna,linha+1));
		}
		if(coluna!=0){
			CTs.add(CasanoQuadrante(coluna-1,linha));
		}
		if(coluna!=LarguraemCasas-1){
			CTs.add(CasanoQuadrante(coluna+1,linha));
		}
		return CTs;
	}
	
	public int distancia(CelulaTerreno C1,CelulaTerreno C2){
		return Math.abs(C1.getColuna()-C2.getColuna())+Math.abs(C1.getLinha()-C2.getLinha());
	}
	
	@Override
	public void explodirBomba(Bomba b, int tempoApagarFogo) {
		if(!b.foiExplodida()){
			CelulaTerreno Loc = CasaCorrespondente(b.localizacao());
			Loc.removerElemento(b);
			b.lancador().removerBombadaLista(b);
			b.setExplodida(true);
			for(CelulaTerreno CT : b.CelulasExplodidas(this)){
				if(CT.getEstado()==TiposElementos.bomba)
					explodirBomba((Bomba) CT.getTopo(),tempoApagarFogo);
				Fogo F = CT.atearFogo();
				MecJ.adicionarFogo(F, CT, System.currentTimeMillis()+tempoApagarFogo);
			}
		}
	}

	@Override
	public int getAlturadeUmaCasa() {
		return AlturadeUmaCasa;
	}

	public int getAlturaemCasas() {
		return AlturaemCasas;
	}

	@Override
	public int getAlturaTotal() {
		return AlturadeUmaCasa*AlturaemCasas;
	}

	@Override
	public List<List<CelulaTerreno>> getCasas() {
		return Casas;
	}

	@Override
	public List<CelulaTerreno> getChaos() {
		List<CelulaTerreno> CTs = new ArrayList<>();
		for(List<CelulaTerreno> L : Casas){
			for(CelulaTerreno CT : L){
				TiposElementos T = CT.getEstado();
				if(T==TiposElementos.chao)
					CTs.add(CT);
			}	
		}
		return CTs;
	}
	
	@Override
	public Collection<CelulaTerreno> getEspeciaisDescobertos() {
		List<CelulaTerreno> CTs = new ArrayList<>();
		for(List<CelulaTerreno> L : Casas){
			for(CelulaTerreno CT : L){
				TiposElementos T = CT.getEstado();
				if(T!=TiposElementos.chao && T!=TiposElementos.paredeInquebravel && T!=TiposElementos.paredeQuebravel)
					CTs.add(CT);
			}	
		}
		return CTs;
	}

	@Override
	public int getLarguradeUmaCasa() {
		return LarguradeUmaCasa;
	}

	public int getLarguraemCasas() {
		return LarguraemCasas;
	}

	@Override
	public int getLarguraTotal() {
		return LarguradeUmaCasa*LarguraemCasas;
	}

	public List<Personagem> getPersonagens() {
		return Personagens;
	}

	@Override
	public void moverBomba(Bomba topo, CelulaTerreno origem,CelulaTerreno destino) {
		origem.removerElemento(topo);
		destino.adicionarElemento(topo);
		topo.setCoordenadas(destino.getCoordenadaCorrespondente());
		topo.setCelulaAtual(destino);
	}

	public void removerPersonagem(Personagem p) {
		Personagens.remove(p);
		
	}

	public void setMecanicaJogo(MecanicaJogo M){
		MecJ = M;
	}
	
}
