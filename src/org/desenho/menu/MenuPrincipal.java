package org.desenho.menu;

import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import org.desenho.CarregadorImagens;
import org.principal.opcoes.Situacao;

public class MenuPrincipal extends JPanel {

	private static final long serialVersionUID = 1L;

	private JButton inicioButton;
	private JButton escolherPersonagem;
	private JButton sair;
	private JButton escolherFase;
	private Situacao situacao;

	

	public MenuPrincipal(Situacao sit) {
		this.situacao = sit;
		BufferedImage inicioImg = CarregadorImagens
				.pegarImagem("start-button.png");
		BufferedImage escolhaImg = CarregadorImagens.pegarImagem("choiceimg.png");
		BufferedImage exitImg = CarregadorImagens.pegarImagem("exit-sign.png");

		inicioButton = new JButton("Come�ar Agora!", new ImageIcon(
				inicioImg.getScaledInstance(100, 100, Image.SCALE_DEFAULT),
				"Start"));
		escolherPersonagem = new JButton("Escolher Personagem", new ImageIcon(
				escolhaImg.getScaledInstance(100, 100, Image.SCALE_DEFAULT),
				"Escolher"));
		escolherFase = new JButton("Escolher Fase");
		sair = new JButton("Sair", new ImageIcon(exitImg.getScaledInstance(100,
				100, Image.SCALE_DEFAULT), "Sair"));
		setLayout(new GridLayout(3, 1, 2, 3));
		inicioButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					situacao.setSituacao(Situacao.jogando);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		escolherPersonagem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					situacao.setSituacao(Situacao.menuPersonagem);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		escolherFase.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					situacao.setSituacao(Situacao.menuFase);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		sair.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					situacao.setSituacao(Situacao.creditos);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		add(inicioButton);
		add(escolherPersonagem);
		add(escolherFase);
		add(sair);
	}

	public void desativaInicio() {
		inicioButton.setEnabled(false);
	}

	public void ativaInicio() {
		inicioButton.setEnabled(true);
	}

	public void desativaPersonagem() {
		escolherPersonagem.setEnabled(false);
	}

	public void ativaPersonagem() {
		escolherPersonagem.setEnabled(true);
	}
	
	public void desativaMenuFase(){
		escolherFase.setEnabled(false);
	}
	
	public void ativaMenuFase(){
		escolherFase.setEnabled(true);
	}

}
