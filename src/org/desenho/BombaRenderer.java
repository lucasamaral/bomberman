package org.desenho;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.bomba.Bomba;
import org.terreno.Coordenada;

public class BombaRenderer extends ElementoTerrenoRenderer {
	protected int numeroQuadros=3;
	protected boolean subindo = true;
	protected BufferedImage[] Bombas = new BufferedImage[numeroQuadros];
	private Bomba B;
	int i = 0;
	private int taxaAtualizacao=100;
	long now = System.currentTimeMillis();
	
	public BombaRenderer(Bomba b) {
		super();
		B = b;
		try{
			BufferedImage BIV = ImageIO.read(new File("bombas.png"));
			for(int i=0;i<numeroQuadros;i++){
				Bombas[i] = BIV.getSubimage(39+20*i, 18, 16, 18);				
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void render(Graphics2D g) {
		long agora = System.currentTimeMillis();
		int delta=(int) (agora-now);
		if(delta>taxaAtualizacao){
			now = agora;
			if(i==2||i==0){
				subindo=!subindo;
				i=1;
			}else if(subindo){
				i=i+1;
			}else{
				i=i-1;
			}
		}
		BufferedImage A = null;		
		A = Bombas[i];
		
		Coordenada C = B.getCelulaAtual().getCoordenadaCorrespondente();
		g.drawImage(A,C.getX(),C.getY(),B.getCelulaAtual().getLargura(),B.getCelulaAtual().getAltura(),null);
	}
		
		
		
		
	

	
	

}
