package org.desenho;

public abstract class ElementoTerrenoRenderer implements Renderer {
	
	protected int largura;
	protected int altura;
	
	public void setLargura(int largura) {
		this.largura = largura;
	}

	public void setAltura(int altura) {
		this.altura = altura;
	}
}
