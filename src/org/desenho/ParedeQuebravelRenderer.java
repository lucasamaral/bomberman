package org.desenho;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import org.elementos.ParedeQuebravel;

public class ParedeQuebravelRenderer extends ElementoTerrenoRenderer {
	
	private BufferedImage A;
	private ParedeQuebravel P;

	public ParedeQuebravelRenderer(ParedeQuebravel paredeQuebravel) {
		P = paredeQuebravel;
		BufferedImage BIV= CarregadorImagens.pegarImagem("paredeQuebravel.png");
		A=BIV.getSubimage(11, 11, 20, 20);
	}

	@Override
	public void render(Graphics2D g) {
		g.drawImage(A,P.localizacao().getX(),P.localizacao().getY(),P.getCelulaAtual().getLargura(),P.getCelulaAtual().getAltura(),null);

	}
}
