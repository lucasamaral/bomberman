package org.desenho;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import org.ovos.Ovo;

public class OvoRenderer extends ElementoTerrenoRenderer {
		
	BufferedImage imagemOvo;
	
	public OvoRenderer(Ovo o) {
		super();
		O = o;
		BufferedImage A = CarregadorImagens.pegarImagem("Eggsprites.png");
		imagemOvo = A.getSubimage(0, 0, 31, 31);
	}

	private Ovo O;
	
	@Override
	public void render(Graphics2D g) {
		g.setColor(Color.BLUE);
		int x = O.localizacao().getX();
		int y = O.localizacao().getY();
		g.drawImage(imagemOvo,x , y, x+20, y+20, 0, 0, 31, 31, null);

	}


}
