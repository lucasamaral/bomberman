package org.desenho;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import org.elementos.ParedeInquebravel;

public class ParedeInquebravelRenderer extends ElementoTerrenoRenderer {
	private BufferedImage A;
	private ParedeInquebravel P;

	public ParedeInquebravelRenderer(ParedeInquebravel paredeInquebravel) {
		P = paredeInquebravel;
		BufferedImage BIV= CarregadorImagens.pegarImagem("celulaCenario.png");
		A=BIV.getSubimage(8, 4, 20, 20);
	}

	@Override
	public void render(Graphics2D g) {
		g.drawImage(A,P.localizacao().getX(),P.localizacao().getY(),P.getCelulaAtual().getLargura(),P.getCelulaAtual().getAltura(),null);


	}

	
	

}
