package org.desenho;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import org.personagem.Personagem;
import org.personagem.Posicionamento;

public class PersonagemRendererPadrao implements PersonagemRenderer{

	protected Personagem P;
	int i = 0;
	int count = 0;
	protected int largura = 20;
	protected int altura = 30;
	protected int numeroQuadros;
	protected BufferedImage[] Baixo = new BufferedImage[numeroQuadros];
	protected BufferedImage[] Cima = new BufferedImage[numeroQuadros];
	protected BufferedImage[] Direita = new BufferedImage[numeroQuadros];
	protected BufferedImage[] Esquerda = new BufferedImage[numeroQuadros];
	protected int taxaAtualizacao = 85;
	protected long now = System.currentTimeMillis();

	public PersonagemRendererPadrao() {
		super();
	}

	@Override
	public void render(Graphics2D g) {
		renderizarXY(g,P.getCoordenada().getX()-largura/2, P.getCoordenada().getY()-altura);
	}
	
	public void renderizarXY(Graphics2D g,int x,int y){
		long agora = System.currentTimeMillis();
		int delta=(int) (agora-now);
		if(delta>taxaAtualizacao){
			now = agora;
			if(P.getPosicionamento()==Posicionamento.PARADO)
				i=0;
			else
				i=(i+delta/taxaAtualizacao)%numeroQuadros;
		}
		BufferedImage A = null;
		switch(P.getUltimoPosicionamento()){
		case CIMA: A = Cima[i];break;
		case BAIXO:A = Baixo[i];break;
		case DIREITA:A = Direita[i];break;
		case ESQUERDA:A = Esquerda[i];break;
		}
		g.drawImage(A, null, x, y);
	}

}