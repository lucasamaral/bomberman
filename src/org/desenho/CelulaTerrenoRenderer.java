package org.desenho;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import org.terreno.CelulaTerreno;

public class CelulaTerrenoRenderer implements Renderer {
	
	CelulaTerreno CT;
	private int largura;
	private int altura;
	protected BufferedImage celula;

	public CelulaTerrenoRenderer(CelulaTerreno celulaTerreno) {
		CT = celulaTerreno;
		largura = celulaTerreno.getLargura();
		altura = celulaTerreno.getAltura();
	}
	
	public CelulaTerrenoRenderer(){
		
	}
	@Override
	public void render(Graphics2D g) {
		g.setColor(Color.darkGray);
		g.fillRect(CT.getColuna() * largura,CT.getLinha() * altura,largura, altura);
		if(CT.getTopo()!=null)
			CT.getTopo().getRenderer().render(g);
	}

}
