package org.caracteristicas;

import org.elementos.TiposElementos;
import org.personagem.Posicionamento;
import org.terreno.Coordenada;

public interface Movimentavel {

	public Posicionamento getPosicionamento();
	public void setPosicionamento(Posicionamento P);
	public int getSpeed();
	public Coordenada getCoordenada();
	public boolean passa(TiposElementos estado);

}
