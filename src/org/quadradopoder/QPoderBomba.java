package org.quadradopoder;

import org.desenho.QuadradoPoderRenderer;
import org.personagem.Personagem;
import org.terreno.CelulaTerreno;

public class QPoderBomba extends QuadradoPoder {

	public QPoderBomba(CelulaTerreno CT){
		super(CT);
		Renderizador = new QuadradoPoderRenderer(this,"Bombupsprite.png");
	}
	
	public QPoderBomba() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void aoAdicionar(Personagem P) {
		P.incNoBombas(1);

	}

	@Override
	public void aoRemover(Personagem P) {
		P.incNoBombas(-1);
	}

}
