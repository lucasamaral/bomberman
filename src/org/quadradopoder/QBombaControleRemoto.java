package org.quadradopoder;

import org.bomba.FabricaBombaControleRemoto;
import org.desenho.QuadradoPoderRenderer;
import org.personagem.Personagem;
import org.poder.PoderDetonarBomba;
import org.terreno.CelulaTerreno;

public class QBombaControleRemoto extends QuadradoPoder{
	
	private FabricaBombaControleRemoto FB;
	private PoderDetonarBomba PDB;
	
	public QBombaControleRemoto(CelulaTerreno casanoQuadrante) {
		super(casanoQuadrante);
		Renderizador = new QuadradoPoderRenderer(this,"Remote_Control_2.png");
	}

	@Override
	public void aoAdicionar(Personagem P) {
		FB = new FabricaBombaControleRemoto();
		PDB = new PoderDetonarBomba(P);
		P.adicionarFabricadeBombas(FB);
		P.adicionarPoder3(PDB);
		
	}

	@Override
	public void aoRemover(Personagem P) {
		P.removerFabricadeBombas(FB);
		P.removerPoder3(PDB);
		
	}

}
