package org.quadradopoder;

import org.desenho.QuadradoPoderRenderer;
import org.elementos.TiposElementos;
import org.personagem.Personagem;
import org.terreno.CelulaTerreno;

public class QAtravessarParede extends QuadradoPoder{

	public QAtravessarParede(CelulaTerreno casanoQuadrante) {
		super(casanoQuadrante);
		Renderizador = new QuadradoPoderRenderer(this,"Soft_Block_Pass.png");
	}

	public QAtravessarParede() {
		Renderizador = new QuadradoPoderRenderer(this,"Soft_Block_Pass.png");
	}

	@Override
	public void aoAdicionar(Personagem P) {
		P.adicionarEstadoAndavel(TiposElementos.paredeQuebravel);
		
	}

	@Override
	public void aoRemover(Personagem P) {
		P.removerEstadoAndavel(TiposElementos.paredeQuebravel);
		
	}

}
