package org.quadradopoder;

import org.desenho.QuadradoPoderRenderer;
import org.personagem.Personagem;
import org.poder.SegurarBomba;
import org.terreno.CelulaTerreno;

public class QSegurarBomba extends QuadradoPoder {
	
	SegurarBomba SB;
	
	public QSegurarBomba(CelulaTerreno casanoQuadrante) {
		super(casanoQuadrante);
		Renderizador = new QuadradoPoderRenderer(this,"Blueglovesprite.png");
	}

	@Override
	public void aoAdicionar(Personagem P) {
		SB = new SegurarBomba(P);
		P.adicionarPoderSegurar(SB);
	}

	@Override
	public void aoRemover(Personagem P) {
		P.removerPoderSegurar(SB);
	}

}
