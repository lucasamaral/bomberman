package org.personagem.fabricas;

import org.personagem.IdentificacaoPersonagem;
import org.personagem.Personagem;
import org.personagem.PersonagemBomberman;
import org.personagem.PersonagemDarkBomberman;
import org.personagem.PersonagemMario;
import org.personagem.PersonagemPateta;
import org.personagem.PersonagemPikachu;

public class FabricaPersonagem extends FabricaPersonagemAbstrata {

	@Override
	public Personagem criaPersonagem(IdentificacaoPersonagem tipo, String playerName) {
		Personagem p;
		switch (tipo) {
		case pateta:
			p = new PersonagemPateta(playerName);
			break;
		case whiteBomberman:
			p = new PersonagemBomberman(playerName);
			break;
		case blackBomberman:
			p = new PersonagemDarkBomberman(playerName);
			break;
		case mario:
			p = new PersonagemMario(playerName);
			break;
		case pikachu:
			p = new PersonagemPikachu(playerName);
			break;
		default:
			p = null;
			break;
		}
		return p;
	}

}
