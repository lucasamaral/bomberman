package org.personagem;

import org.desenho.PersonagemBombermanRenderer;
import org.desenho.PersonagemRendererPadrao;

public class PersonagemBomberman extends PersonagemPadrao {
	private PersonagemRendererPadrao Renderizador = new PersonagemBombermanRenderer(this);
	
	public PersonagemBomberman(String nome) {
		super(nome);
	}
	@Override
	public PersonagemRendererPadrao getRenderer() {
		return Renderizador;
	}
}
