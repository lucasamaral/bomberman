package org.personagem;

import org.desenho.PersonagemMarioRenderer;
import org.desenho.PersonagemRendererPadrao;

public class PersonagemMario extends PersonagemPadrao {

	private PersonagemRendererPadrao Renderizador = new PersonagemMarioRenderer(this);
	
	public PersonagemMario(String nome) {
		super(nome);
	}

	@Override
	public PersonagemRendererPadrao getRenderer() {
		return Renderizador;
	}

}
