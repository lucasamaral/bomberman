package org.personagem;

import org.desenho.PersonagemPatetaRenderer;
import org.desenho.PersonagemRendererPadrao;

public class PersonagemPateta extends PersonagemPadrao {

	private PersonagemRendererPadrao Renderizador = new PersonagemPatetaRenderer(this);
	
	public PersonagemPateta(String nome) {
		super(nome);
	}

	@Override
	public PersonagemRendererPadrao getRenderer() {
		return Renderizador;
	}

}
