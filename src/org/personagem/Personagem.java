package org.personagem;

import java.util.List;

import org.bomba.Bomba;
import org.bomba.FabricadeBombas;
import org.caracteristicas.Movimentavel;
import org.caracteristicas.Seguravel;
import org.desenho.PersonagemRenderer;
import org.elementos.TiposElementos;
import org.poder.Poder;
import org.poder.PoderSegurar;
import org.quadradopoder.QuadradoPoder;
import org.terreno.CelulaTerreno;
import org.terreno.Coordenada;

public interface Personagem extends Movimentavel {
	
	public void adicionaBomba(Bomba b);
	public void adicionarFabricadeBombas(FabricadeBombas FB);
	public void adicionarPoder(QuadradoPoder topo);
	public void adicionarPoder1(Poder k);
	public void adicionarPoder2(Poder k);
	public void adicionarPoder3(Poder k);
	public void adicionarPoderSegurar(PoderSegurar sB);
	public Personagem eliminar();
	public Bomba getBomba();
	public List<Bomba> getBombasColocadas();
	public PoderSegurar getPoderSegurar();
	public PersonagemRenderer getRenderer();
	public Seguravel getSegurado();
	public Posicionamento getUltimoPosicionamento();
	public void incNoBombas(int valor);
	public void incPoderdeFogo(int valor);
	public void incvelocidade(int valor);
	public boolean passa(TiposElementos tiposElementos);
	public Poder getPoder1();
	public Poder getPoder2();
	public Poder getPoder3();
	public boolean PossuiBombas();
	public void removerBombadaLista(Bomba b);
	public void removerFabricadeBombas(FabricadeBombas FB);
	public void removerPoder1(Poder k);
	public void removerPoderSegurar(PoderSegurar sB);
	public void segurar(Seguravel tirarTopo);
	public void setCoordenada(Coordenada Novo);
	public boolean sobrevive(TiposElementos tiposElementos);
	public void soltar(CelulaTerreno cT);
	void removerPoder2(Poder k);
	void removerPoder3(Poder k);
	public void adicionarEstadoAndavel(TiposElementos paredequebravel);
	public void removerEstadoAndavel(TiposElementos paredequebravel);
	public String getNome();
	public boolean estaMorto();
	public boolean eliminado();
	
}
