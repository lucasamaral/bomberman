package org.personagem;

import java.util.List;

import org.bomba.Bomba;
import org.bomba.FabricadeBombas;
import org.caracteristicas.Seguravel;
import org.desenho.PersonagemOvoRenderer;
import org.desenho.PersonagemRenderer;
import org.elementos.TiposElementos;
import org.poder.Poder;
import org.poder.PoderSegurar;
import org.quadradopoder.QuadradoPoder;
import org.terreno.CelulaTerreno;
import org.terreno.Coordenada;

public class PersonagemOvo implements Personagem {
	
	Personagem interno;
	PersonagemOvoRenderer Renderizador = new PersonagemOvoRenderer(this);

	public PersonagemOvo(Personagem Dentro) {
		interno = Dentro;
	}
	
	public void adicionaBomba(Bomba b){
		interno.adicionaBomba(b);
	}
	
	@Override
	public void adicionarFabricadeBombas(FabricadeBombas FB){
		interno.adicionarFabricadeBombas(FB);
	}
	
	public void adicionarPoder(QuadradoPoder topo){
		interno.adicionarPoder(topo);
	}
	
	@Override
	public void adicionarPoder1(Poder k) {
		interno.adicionarPoder1(k);
		
	}
	
	@Override
	public void adicionarPoderSegurar(PoderSegurar sB) {
		interno.adicionarPoderSegurar(sB);
		
	}

	@Override
	public Personagem eliminar(){
		return interno;
	}

	@Override
	public Bomba getBomba(){
		return interno.getBomba();
	}

	@Override
	public List<Bomba> getBombasColocadas() {
		return interno.getBombasColocadas();
	}

	@Override
	public Coordenada getCoordenada(){
		return interno.getCoordenada();
	}
	
	@Override
	public Poder getPoder1() {
		return interno.getPoder1();
	}
	
	@Override
	public Poder getPoder2() {
		return interno.getPoder2();
	}
	
	@Override
	public Poder getPoder3() {
		return interno.getPoder3();
	}
	
	@Override
	public PoderSegurar getPoderSegurar() {
		return interno.getPoderSegurar();
	}
	
	@Override
	public Posicionamento getPosicionamento() {
		return interno.getPosicionamento();
	}
	
	@Override
	public PersonagemRenderer getRenderer() {
		return Renderizador;
	}

	@Override
	public Seguravel getSegurado() {
		return interno.getSegurado();
	}

	@Override
	public int getSpeed(){
		return interno.getSpeed();
	}

	@Override
	public Posicionamento getUltimoPosicionamento() {
		return interno.getUltimoPosicionamento();
	}

	public void incNoBombas(int valor){
		interno.incNoBombas(valor);
	}

	public void incPoderdeFogo(int valor){
		interno.incPoderdeFogo(valor);
	}

	@Override
	public void incvelocidade(int valor) {
		interno.incvelocidade(valor);
	}
	
	@Override
	public boolean passa(TiposElementos estado) {
		return interno.passa(estado);
	}
	
	@Override
	public boolean PossuiBombas(){	
		return interno.PossuiBombas();
	}
	
	public void removerBombadaLista(Bomba B){
		interno.removerBombadaLista(B);
	}

	@Override
	public void removerFabricadeBombas(FabricadeBombas FB){
		interno.removerFabricadeBombas(FB);
	}

	@Override
	public void removerPoder1(Poder k) {
		interno.removerPoder1(k);
		
	}

	@Override
	public void removerPoderSegurar(PoderSegurar sB) {
		interno.removerPoderSegurar(sB);
		
	}

	@Override
	public void segurar(Seguravel tirarTopo) {
		interno.segurar(tirarTopo);
		
	}

	@Override
	public void setCoordenada(Coordenada Novo){
		interno.setCoordenada(Novo);
	}

	@Override
	public void setPosicionamento(Posicionamento P) {
		interno.setPosicionamento(P);
		
	}

	@Override
	public boolean sobrevive(TiposElementos estado) {
		return interno.sobrevive(estado);
	}

	@Override
	public void soltar(CelulaTerreno cT) {
		interno.soltar(cT);
		
	}

	@Override
	public void adicionarPoder2(Poder k) {
		interno.adicionarPoder2(k);
	}

	@Override
	public void adicionarPoder3(Poder k) {
		interno.adicionarPoder3(k);
		
	}

	@Override
	public void removerPoder2(Poder k) {
		interno.removerPoder2(k);
	}

	@Override
	public void removerPoder3(Poder k) {
		interno.removerPoder3(k);
	}

	@Override
	public void adicionarEstadoAndavel(TiposElementos paredequebravel) {
		interno.adicionarEstadoAndavel(paredequebravel);
		
	}

	@Override
	public void removerEstadoAndavel(TiposElementos paredequebravel) {
		interno.adicionarEstadoAndavel(paredequebravel);
	}

	public Personagem getPersonagemInterno() {
		return interno;
	}

	@Override
	public String getNome() {
		return interno.getNome();
	}

	@Override
	public boolean estaMorto() {
		return interno.estaMorto();
	}

	@Override
	public boolean eliminado() {
		return interno.eliminado();
	}

}
