package org.mecanica;

import java.util.LinkedList;
import java.util.List;

import org.caracteristicas.Movimentavel;
import org.personagem.Personagem;
import org.terreno.CelulaTerreno;
import org.terreno.Coordenada;

public class Movimentador implements Runnable {
	
	private MecanicaJogo MJ;
	private List<Movimentavel> Movimentaveis = new LinkedList<>();
	
	public Movimentador(MecanicaJogo mecanicaJogo) {
		MJ = mecanicaJogo;
	}

	@Override
	public void run() {
		while(!MJ.FimdeJogo()){
			for(Personagem P : MJ.getCenario().getPersonagens()){
				movimentar(P);
			}
			for(Movimentavel M : Movimentaveis)
				movimentar(M);
			try {
				Thread.sleep(60);
			} catch (InterruptedException e) {
			}
		}
	}

	private void movimentar(Movimentavel p) {
		switch(p.getPosicionamento()){
			case CIMA : movimentarCima(p) ;break;
			case BAIXO : movimentarBaixo(p); break;
			case DIREITA : movimentarDireita(p) ;break;
			case ESQUERDA : movimentarEsquerda(p); break;
		}
		
	}

	private void movimentarEsquerda(Movimentavel p) {
		int v = p.getSpeed();
		tentarinc(p,-v,0);
	}

	private void movimentarDireita(Movimentavel p) {
		int v = p.getSpeed();
		tentarinc(p,v,0);
	}

	private void movimentarBaixo(Movimentavel p) {
		int v = p.getSpeed();
		tentarinc(p,0,v);
	}

	private void movimentarCima(Movimentavel p) {
		int v = p.getSpeed();
		tentarinc(p,0,-v);
	}
	
	private void tentarinc(Movimentavel P,int x, int y){
		Coordenada Nova = P.getCoordenada().clone();
		Nova.inc(x, y);
		if(PersonagemPodeir(P,Nova))
			P.getCoordenada().inc(x, y);
	}
	
	public boolean PersonagemPodeir(Movimentavel P, Coordenada Cnova){
		CelulaTerreno C1 = MJ.getCenario().CasaCorrespondente(P.getCoordenada());
		CelulaTerreno C2 = MJ.getCenario().CasaCorrespondente(Cnova);
		if(C1==C2||P.passa(C2.getEstado()))
			return true;
		return false;
		
	}

}
