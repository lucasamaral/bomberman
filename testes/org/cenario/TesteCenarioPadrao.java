package org.cenario;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.terreno.CelulaTerreno;
import org.terreno.Coordenada;

public class TesteCenarioPadrao {

	Cenario C;
	
	@Before
	public void setUp() throws Exception {
		C = CenarioTeste.getCenarioTeste();
	}

	@Test
	public void testeDimensoesCenario(){
		assertEquals(C.getLarguradeUmaCasa(),20);
		assertEquals(C.getAlturaemCasas(),7);
		assertEquals(C.getLarguradeUmaCasa(),20);
		assertEquals(C.getLarguraemCasas(),7);
		assertEquals(C.getAlturaTotal(),140);
		assertEquals(C.getLarguraTotal(),140);
	}
	
	@Test
	public void TesteCelulasVizinhasMeio(){
		Set<CelulaTerreno> NN = new HashSet<CelulaTerreno>(C.CelulasVizinhas(C.CasanoQuadrante(3, 3)));
		Set<CelulaTerreno> Ref = new HashSet<>();
		Ref.add(C.CasanoQuadrante(4, 3));
		Ref.add(C.CasanoQuadrante(2, 3));
		Ref.add(C.CasanoQuadrante(3, 4));
		Ref.add(C.CasanoQuadrante(3, 2));
		assertEquals(Ref,NN);
	}
	
	@Test
	public void TesteCelulasVizinhasCanto(){
		Set<CelulaTerreno> NN = new HashSet<CelulaTerreno>(C.CelulasVizinhas(C.CasanoQuadrante(0, 0)));
		Set<CelulaTerreno> Ref = new HashSet<>();
		Ref.add(C.CasanoQuadrante(1, 0));
		Ref.add(C.CasanoQuadrante(0, 1));
		assertEquals(Ref,NN);
	}
	
	@Test
	public void TesteCelulasVizinhasAlto(){
		Set<CelulaTerreno> NN = new HashSet<CelulaTerreno>(C.CelulasVizinhas(C.CasanoQuadrante(2, 0)));
		Set<CelulaTerreno> Ref = new HashSet<>();
		Ref.add(C.CasanoQuadrante(1, 0));
		Ref.add(C.CasanoQuadrante(2, 1));
		Ref.add(C.CasanoQuadrante(3, 0));
		assertEquals(Ref,NN);
	}
	
	@Test
	public void TesteCelulaAcimaAbaixoDirEsqCelulaCima(){
		CelulaTerreno CT = C.CasanoQuadrante(2, 0);
		assertEquals(C.CelulaAcima(CT),C.CasanoQuadrante(2, 6));
		assertEquals(C.CelulaAbaixo(CT),C.CasanoQuadrante(2, 1));
		assertEquals(C.CelulaDireita(CT),C.CasanoQuadrante(3, 0));
		assertEquals(C.CelulaEsquerda(CT),C.CasanoQuadrante(1, 0));
	}
	
	@Test
	public void TesteCelulaAcimaAbaixoDirEsqCelulaMeio(){
		CelulaTerreno CT = C.CasanoQuadrante(4, 3);
		assertEquals(C.CelulaAcima(CT),C.CasanoQuadrante(4, 2));
		assertEquals(C.CelulaAbaixo(CT),C.CasanoQuadrante(4, 4));
		assertEquals(C.CelulaDireita(CT),C.CasanoQuadrante(5, 3));
		assertEquals(C.CelulaEsquerda(CT),C.CasanoQuadrante(3, 3));
	}
	
	@Test
	public void TesteCelulaAcimaAbaixoDirEsqCelulaCanto(){
		CelulaTerreno CT = C.CasanoQuadrante(6, 0);
		assertEquals(C.CasanoQuadrante(6, 6),C.CelulaAcima(CT));
		assertEquals(C.CasanoQuadrante(6, 1),C.CelulaAbaixo(CT));
		assertEquals(C.CasanoQuadrante(0, 0),C.CelulaDireita(CT));
		assertEquals(C.CasanoQuadrante(5, 0),C.CelulaEsquerda(CT));
	}
	
	@Test
	public void TesteDetCoordenadas(){
		CelulaTerreno CT = C.CasaCorrespondente(new Coordenada(53,10));
		assertEquals(CT,C.CasanoQuadrante(2, 0));
	}
	
	@Test
	public void TesteDetCoordenadasNotaveis(){
		CelulaTerreno CT = C.CasaCorrespondente(new Coordenada(0,0));
		assertEquals(CT,C.CasanoQuadrante(0, 0));
		CelulaTerreno CC = C.CasaCorrespondente(new Coordenada(139,139));
		assertEquals(CC,C.CasanoQuadrante(6, 6));
	}
	
	@Test
	public void TesteMesmaLinha(){
		Set<CelulaTerreno> NN = new HashSet<CelulaTerreno>(C.CelulasMesmaLinha(C.CasanoQuadrante(2, 0)));
		Set<CelulaTerreno> Ref = new HashSet<>();
		Ref.add(C.CasanoQuadrante(1, 0));
		Ref.add(C.CasanoQuadrante(2, 0));
		Ref.add(C.CasanoQuadrante(3, 0));
		Ref.add(C.CasanoQuadrante(4, 0));
		Ref.add(C.CasanoQuadrante(5, 0));
		Ref.add(C.CasanoQuadrante(6, 0));
		Ref.add(C.CasanoQuadrante(0, 0));
		assertEquals(Ref,NN);
	}
	
	@Test
	public void TesteMesmaColuna(){
		Set<CelulaTerreno> NN = new HashSet<CelulaTerreno>(C.CelulasMesmaColuna(C.CasanoQuadrante(5, 3)));
		Set<CelulaTerreno> Ref = new HashSet<>();
		Ref.add(C.CasanoQuadrante(5, 0));
		Ref.add(C.CasanoQuadrante(5, 1));
		Ref.add(C.CasanoQuadrante(5, 2));
		Ref.add(C.CasanoQuadrante(5, 3));
		Ref.add(C.CasanoQuadrante(5, 4));
		Ref.add(C.CasanoQuadrante(5, 5));
		Ref.add(C.CasanoQuadrante(5, 6));
		assertEquals(Ref,NN);
	}
}
