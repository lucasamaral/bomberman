package org.bomba;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.HashSet;
import java.util.Set;

import org.cenario.Cenario;
import org.cenario.CenarioTeste;
import org.junit.Before;
import org.junit.Test;
import org.terreno.CelulaTerreno;
import org.terreno.Coordenada;

public class TesteBombaControleRemoto {

	private Cenario C;
	
	@Before
	public void setUp() throws Exception {
		C = CenarioTeste.getCenarioTeste();
	}
	
	@Test
	public void testenaoExplodeporTempo(){
		Bomba B = new BombaControleRemoto(2,new Coordenada(30,30));
		assertFalse(B.explodeporTempo());
	}
	
	@Test
	public void testBombaCanto() {
		Bomba B = new BombaControleRemoto(2,new Coordenada(30,30));
		Set<CelulaTerreno> NN = B.CelulasExplodidas(C);
		Set<CelulaTerreno> Ref = new HashSet<>();
		Ref.add(C.CasanoQuadrante(1, 1));
		Ref.add(C.CasanoQuadrante(2, 1));
		Ref.add(C.CasanoQuadrante(1, 2));
		Ref.add(C.CasanoQuadrante(1, 3));
		assertEquals(Ref,NN);
	}

}
